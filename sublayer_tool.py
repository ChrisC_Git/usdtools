'''
Simple UI app to allow users to:
1) Create a new usd sublayer file containing 2 other usd files
2) Add a usd file to an exisiting sublayered .usd file in a specific position

Build to exe with 'pyinstaller --onefile --windowed --icon icon.ico sublayer_tool.py'
'''
import os
import tkinter as tk
from tkinter import messagebox, filedialog

def read_usda_file(filename):
    with open(filename, 'r') as file:
        return file.read()

def write_usda_file(filename, content):
    with open(filename, 'w') as file:
        file.write(content)

def usd_relpath(relative_path):
    '''
    Convert a given relative path to a normalized USD-compatible format.

    USD paths often use forward slashes and may require specific handling
    for relative paths. This function ensures that the input path conforms
    to USD path conventions.
    '''
    if relative_path == os.path.basename(relative_path):
        relative_path = './' + relative_path
    elif not relative_path.startswith('..\\'): # or ../?
        relative_path = './' + relative_path
    
    relative_path = relative_path.replace('\\', '/')
    return relative_path

def insert_sublayer(content, new_sublayer, position):
    '''
    Insert a new sublayer into the `subLayers` list in the given content.

    Args:
        content (str):      The ascii text data from the usda file
        new_sublayer (str): The new sublayer path to insert.
        position (int):     The index in the `subLayers` list where the sublayer wil be inserted.

    Returns:
        str: The updated content with the new sublayer inserted at the specified
             position within the `subLayers` list.

    Note:
        The function assumes that the content contains lines with 'subLayers = ['
        and ']' to identify the boundaries of the subLayers list.
    '''
    lines = content.split('\n')
    start_index = next(i for i, line in enumerate(lines) if 'subLayers = [' in line)
    end_index = next(i for i, line in enumerate(lines) if ']' in line and i > start_index)
    sublayer_lines = lines[start_index + 1:end_index]
    sublayer_lines.insert(position, '        ' + new_sublayer)
    lines[start_index + 1:end_index] = sublayer_lines
    return '\n'.join(lines)

def add_sublayer():
    usda_file = file_entry.get()
    new_sublayer_path = sublayer_entry.get()
    if add_sub_relpaths_var.get():
        new_sublayer_path = usd_relpath(os.path.relpath(new_sublayer_path, os.path.dirname(usda_file)))
    new_sublayer = '@'+ new_sublayer_path +'@'
    try:
        position = int(position_entry.get())
    except ValueError:
        messagebox.showerror("Error", "Position must be an integer.")
        return
    
    if usda_file and new_sublayer and position is not None:
        try:
            usda_content = read_usda_file(usda_file)
            usda_content = insert_sublayer(usda_content, new_sublayer, position)
            write_usda_file(usda_file, usda_content)
            messagebox.showinfo("Success", "Sublayer added successfully!")
        except Exception as e:
            messagebox.showerror("Error", f"An error occurred: {e}")
    else:
        messagebox.showerror("Error", "All fields are required.")

def combine_sublayers():
    sublayer1 = sublayer1_entry.get()
    sublayer2 = sublayer2_entry.get()
    output_file = output_entry.get()
    
    if sublayer1 and sublayer2 and output_file:
        if use_relative_paths_var.get():
            sublayer1 = usd_relpath(os.path.relpath(sublayer1, os.path.dirname(output_file)))
            sublayer2 = usd_relpath(os.path.relpath(sublayer2, os.path.dirname(output_file)))
        usda_content = f"""#usda 1.0
(
    subLayers = [
        @{sublayer1}@
        @{sublayer2}@
    ]
)"""
        try:
            write_usda_file(output_file, usda_content)
            messagebox.showinfo("Success", "USD file created successfully!")
        except Exception as e:
            messagebox.showerror("Error", f"An error occurred: {e}")
    else:
        messagebox.showerror("Error", "All fields are required.")

def select_file(entry_widget):
    filename = filedialog.askopenfilename(defaultextension=".usd*", filetypes=[("All USD files", "*.usd*"), ("USD files", "*.usd"), ("USDA files", "*.usda"), ("USDC files", "*.usdc"), ("USDZ files", "*.usdz")])
    if filename:
        entry_widget.delete(0, tk.END) # Clear any previous entry
        entry_widget.insert(0, filename)

def save_as(output_entry):
    file_path = filedialog.asksaveasfilename(defaultextension=".usd", filetypes=[("USD files", "*.usd"), ("All files", "*.*")])
    if file_path:
        output_entry.delete(0, tk.END) # Clear any previous entry
        output_entry.insert(tk.END, file_path)

# Create the main window
root = tk.Tk()
root.title("USD Sublayer Tool")

# Create and place the first sublayer entry for combining
tk.Label(root, text="First Sublayer:").grid(row=0, column=0, padx=10, pady=5, sticky='e')
sublayer1_entry = tk.Entry(root, width=50)
sublayer1_entry.grid(row=0, column=1, padx=10, pady=5)
tk.Button(root, text="Browse", command=lambda: select_file(sublayer1_entry)).grid(row=0, column=2, padx=10, pady=5)

# Create and place the second sublayer entry for combining
tk.Label(root, text="Second Sublayer:").grid(row=1, column=0, padx=10, pady=5, sticky='e')
sublayer2_entry = tk.Entry(root, width=50)
sublayer2_entry.grid(row=1, column=1, padx=10, pady=5)
tk.Button(root, text="Browse", command=lambda: select_file(sublayer2_entry)).grid(row=1, column=2, padx=10, pady=5)

# Create and place the output file entry
tk.Label(root, text="Output USD file:").grid(row=2, column=0, padx=10, pady=5, sticky='e')
output_entry = tk.Entry(root, width=50)
output_entry.grid(row=2, column=1, padx=10, pady=5)
tk.Button(root, text="Save As", command=lambda: save_as(output_entry)).grid(row=2, column=2, padx=10, pady=5)

# Checkbox for relative paths
use_relative_paths_var = tk.BooleanVar()
use_relative_paths_checkbox = tk.Checkbutton(root, text="Use Relative Paths", variable=use_relative_paths_var)
use_relative_paths_checkbox.grid(row=3, column=1, padx=10, pady=5, sticky='e')

# Add a button to combine sublayers
combine_button = tk.Button(root, text="Sublayer Files", command=combine_sublayers)
combine_button.grid(row=3, column=0, columnspan=3, pady=10)

# Create a Frame with a sunken relief (or raised if preferred)
divider_frame = tk.Frame(root, height=2, relief="sunken", bg="gray")
divider_frame.grid(row=4, column=0, columnspan=3, sticky="ew", padx=10, pady=10)

# Create and place the USDA file path entry
tk.Label(root, text="Input USD file:").grid(row=5, column=0, padx=10, pady=5, sticky='e')
file_entry = tk.Entry(root, width=50)
file_entry.grid(row=5, column=1, padx=10, pady=5)
tk.Button(root, text="Browse", command=lambda: select_file(file_entry)).grid(row=5, column=2, padx=10, pady=5)

# Create and place the sublayer path entry
tk.Label(root, text="New Sublayer:").grid(row=6, column=0, padx=10, pady=5, sticky='e')
sublayer_entry = tk.Entry(root, width=50)
sublayer_entry.grid(row=6, column=1, padx=10, pady=5)
tk.Button(root, text="Browse", command=lambda: select_file(sublayer_entry)).grid(row=6, column=2, padx=10, pady=5)

# Create and place the position entry
tk.Label(root, text="Insert Position:").grid(row=7, column=0, padx=10, pady=5, sticky='e')
position_entry = tk.Entry(root, width=19)
position_entry.grid(row=7, column=1, padx=10, pady=5, sticky='w')
tk.Label(root, text="(Top = 0)").grid(row=7, column=1)

# Checkbox for relative paths
add_sub_relpaths_var = tk.BooleanVar()
use_relative_paths_checkbox = tk.Checkbutton(root, text="Use Relative Paths", variable=add_sub_relpaths_var)
use_relative_paths_checkbox.grid(row=8, column=1, padx=10, pady=5, sticky='e')

# Add a button to add the sublayer
add_button = tk.Button(root, text="Add Sublayer", command=add_sublayer)
add_button.grid(row=8, column=0, columnspan=3, pady=10)

# Run the application
root.mainloop()