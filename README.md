# USD Tools
A selection of small tools to help artists work with USD without needing too get to deeply involved in the code side of it all.

## Sublayer Tool
![USD Sublayer Tool](http://cunnington.co.za/blog/images/sublayer_tool.png)

Currently it only has one tool **sublayer_tool.py** which easily lets you combine two usd files into a single sublayered usd. It also lets you take a usd file and sublayer in more layers in specific positions. It is built without requiring usd-core installed as it is all just simple text manipulation, this is just to keep this tool as simple as possible.

## Getting started
If you have python installed it should be as easy as double clicking the .py file. I will probably not store the .exe files here on the git repo but if I decide to upload them anywhere I will add download links to this README.

## To Do:
### Sublayer Tool
- [ ] Check input files are usd files
- [x] Check box to allow user to use relative paths
- [x] Convert the 'Output USD file' to 'Save As' option